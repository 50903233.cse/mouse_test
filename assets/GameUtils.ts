


export default class GameUtils {

  public static createItemFromNode<T>(item: new () => T, node: cc.Node, parentNode: cc.Node = null, insert: boolean = false): T {
    if (!node) return null;
    let parent = parentNode ? parentNode : node.parent;
    let newNode = cc.instantiate(node);

    if (insert) {
      parent.insertChild(newNode, 0);
    } else {
      parent.addChild(newNode);
    }
    let newItem = newNode.getComponent(item);
    newItem.node.active = true;
    node.active = false;
    return newItem;
  }

  public static cloneItemFromNode<T>(item: new () => T, node: cc.Node): T {
    if (!node) return null;
    let newNode = cc.instantiate(node);

    let newItem = newNode.getComponent(item);
    newItem.node.active = true;
    node.active = false;
    return newItem;
  }
}
