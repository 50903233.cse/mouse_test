// Learn TypeScript:
//  - https://docs.cocos.com/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

import GameUtils from "./GameUtils";
import ObjTest from "./ObjTest";
import SlotTest from "./SlotTest";
import UIItemTest from "./UIItemTest";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Test extends cc.Component {

    @property(ObjTest)
    obj: ObjTest = null;

    @property(SlotTest)
    slots: SlotTest[] = [];

    @property(UIItemTest)
    item: UIItemTest = null;

    items: Map<string, UIItemTest> = new Map<string, UIItemTest>();

    objs: Map<string, ObjTest> = new Map<string, ObjTest>();

    private curUIIndex:number = 0;
    private curObj: ObjTest = null;
    private curItem: UIItemTest = null;
    private curSlot: SlotTest = null;
    private moveSlot: SlotTest = null;
    start() {
        this.obj.node.active = false;
        this.item.node.active = false;
        for (let index = 0; index < 10; index++) {
            this.createItem(index);
        }

        this.slots.forEach(slot => {
            slot.mouseEnter = () => {
                this.curSlot = slot;
            };

            slot.mouseLeave = () => {
                this.curSlot = null;
            }

            slot.mouseStart = (e) => {
                this.curObj = slot.obj;
                this.moveSlot = slot;
                this._onTouchBegan(e, slot.node.getPosition());
            };

            slot.mouseMove = (e) => {
                this.moveSlot = slot;
                this._onTouchMoved(e, slot.node.getPosition());
            };

            slot.mouseEnd = (e) => {
                this._onTouchEnded(e);
            };
        });
    }

    createItem(index: number) {
        let item = GameUtils.cloneItemFromNode(UIItemTest, this.item.node);
        item.node.setParent(this.item.node.parent);
        item.setData(index);
        item.mouseStart = (e) => {
            this.curUIIndex = item.node.getSiblingIndex();
            this.curItem = item;
            item.node.opacity = 0;
            item.node.setParent(this.obj.node.parent);
            this.createObj(item.index);
            this._onTouchBegan(e);
        };

        item.mouseMove = (e) => {
            this._onTouchMoved(e);
        };

        item.mouseEnd = (e) => {
            this._onTouchEnded(e);
        };
        this.items.set(index + "", item);
    }

    createObj(index: number) {
        let item = GameUtils.cloneItemFromNode(ObjTest, this.obj.node);
        item.node.setParent(this.obj.node.parent);
        item.setData(index);
        this.curObj = item;
        this.objs.set(index + "", item);
    }

    private getPos(event: cc.Event.EventTouch, addPos: cc.Vec2 = cc.Vec2.ZERO) {
        let canvas = cc.director.getScene().getChildByName("Canvas");
        let size = canvas.getContentSize();
        let posE = event.getLocation();
        let pos = new cc.Vec2(posE.x, posE.y);
        pos.subSelf(new cc.Vec2(size.width / 2, size.height / 2))
        pos.subSelf(addPos);
        return new cc.Vec2(pos.x, pos.y);
    }

    private _onTouchBegan(event: cc.Event.EventTouch, addPos: cc.Vec2 = cc.Vec2.ZERO) {
        if (!this.enabledInHierarchy) return;

        this.curObj.node.setPosition(this.getPos(event, addPos));
    }

    private _onTouchMoved(event: cc.Event.EventTouch, addPos: cc.Vec2 = cc.Vec2.ZERO) {
        if (!this.enabledInHierarchy) return;
        if (this.curObj == null) return;
        let pos = this.getPos(event, addPos);
        this.curObj.node?.setPosition(pos);
    }

    private _onTouchEnded(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        if (this.curObj != null) {
            if (this.curSlot != null) {
                if (this.moveSlot != null) {
                    if (this.curSlot.obj != null) {
                        let a = this.curObj;
                        let b = this.curSlot.obj;
                        this.curSlot.setData(a);
                        this.moveSlot.setData(b);

                    } else {
                        let a = this.curObj;
                        this.curSlot.setData(a);
                        this.moveSlot.setData(null);
                    }
                } else {
                    if (this.curSlot.obj != null) {
                        let a = this.curObj;
                        let b = this.curSlot.obj;
                        this.curSlot.setData(a);
                        if (this.curItem != null) {
                            this.curItem.setData(b.index);
                            this.curItem.node.opacity = 255;
                            this.curItem.node.parent = this.item.node.parent;
                            this.curItem.node.setSiblingIndex(this.curUIIndex);
                            b.node.destroy();
                        }
                    }else{
                        let a = this.curObj;
                        this.curSlot.setData(a);
                        if (this.curItem != null) {
                            this.curItem.node.destroy();
                        }
                    }
                }
            } else {
                if (this.moveSlot != null) {
                    this.moveSlot.setData(this.curObj);
                } else {
                    this.curObj.node?.destroy();
                    this.objs.delete(this.curObj.index + "");
                    if (this.curItem != null) {
                        this.curItem.node.opacity = 255;
                        this.curItem.node.parent = this.item.node.parent;
                        this.curItem.node.setSiblingIndex(this.curUIIndex);
                    }
                }
            }
        }
        this.curObj = null;
        this.curSlot = null;
        this.moveSlot = null;
        this.curItem = null;
    }
}
