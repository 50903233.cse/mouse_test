import ObjTest from "./ObjTest";

const {ccclass, property} = cc._decorator;

@ccclass
export default class SlotTest extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property()
    index: number = 0;
    
    obj:ObjTest = null;

    public mouseEnter:(event: cc.Event.EventTouch)=>void = null;
    public mouseLeave:(event: cc.Event.EventTouch)=>void = null;
    public mouseStart:(event: cc.Event.EventTouch)=>void = null;
    public mouseMove:(event: cc.Event.EventTouch)=>void = null;
    public mouseEnd:(event: cc.Event.EventTouch)=>void = null;

    protected onDisable(): void {
        this._unregisterEvent();
    }
    protected onEnable(): void {
        this._registerEvent();
    }

    start(){
        this.label.string = this.index + "";
    }

    setData (obj:ObjTest) {
        this.obj = obj;
        if(this.obj == null) return;
        obj.node.setParent(this.node);
        obj.node.setPosition(cc.Vec2.ZERO);
    }


    private _registerEvent() {
        this.node.on(cc.Node.EventType.MOUSE_ENTER, this._onEnter, this, true);
        this.node.on(cc.Node.EventType.MOUSE_LEAVE, this._onLeave, this, true);
        this.node.on(cc.Node.EventType.TOUCH_START, this._onTouchBegan, this, true);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this._onTouchMoved, this, true);
        this.node.on(cc.Node.EventType.TOUCH_END, this._onTouchEnded, this, true);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this._onTouchCancelled, this, true);
    }

    private _unregisterEvent() {
        this.node.off(cc.Node.EventType.MOUSE_ENTER, this._onEnter, this, true);
        this.node.off(cc.Node.EventType.MOUSE_LEAVE, this._onLeave, this, true);
        this.node.off(cc.Node.EventType.TOUCH_START, this._onTouchBegan, this, true);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this._onTouchMoved, this, true);
        this.node.off(cc.Node.EventType.TOUCH_END, this._onTouchEnded, this, true);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this._onTouchCancelled, this, true);
    }

    private _onEnter(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        this.node.color = cc.Color.YELLOW;
        if(this.mouseEnter)this.mouseEnter(event);
    }

    private _onLeave(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        this.node.color = cc.Color.RED;
        if(this.mouseLeave)this.mouseLeave(event);
    }

    private _onTouchBegan(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        if(this.obj == null) return;
        if(this.mouseStart) this.mouseStart(event);
    }

    private _onTouchMoved(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        if(this.obj == null) return;
        if(this.mouseMove) this.mouseMove(event);
    }

    private _onTouchEnded(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        if(this.obj == null) return;
        if(this.mouseEnd) this.mouseEnd(event);
    }

    private _onTouchCancelled(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        if(this.obj == null) return;
        if(this.mouseEnd) this.mouseEnd(event);
    }
}
