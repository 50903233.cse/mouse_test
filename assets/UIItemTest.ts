const { ccclass, property } = cc._decorator;

@ccclass
export default class UIItemTest extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    public mouseStart:(event: cc.Event.EventTouch)=>void = null;
    public mouseMove:(event: cc.Event.EventTouch)=>void = null;
    public mouseEnd:(event: cc.Event.EventTouch)=>void = null;

    public index:number = 0;
    protected onDisable(): void {
        this._unregisterEvent();
    }
    protected onEnable(): void {
        this._registerEvent();
    }

    private _registerEvent() {
        this.node.on(cc.Node.EventType.TOUCH_START, this._onTouchBegan, this, true);
        this.node.on(cc.Node.EventType.TOUCH_MOVE, this._onTouchMoved, this, true);
        this.node.on(cc.Node.EventType.TOUCH_END, this._onTouchEnded, this, true);
        this.node.on(cc.Node.EventType.TOUCH_CANCEL, this._onTouchCancelled, this, true);
    }

    private _unregisterEvent() {
        this.node.off(cc.Node.EventType.TOUCH_START, this._onTouchBegan, this, true);
        this.node.off(cc.Node.EventType.TOUCH_MOVE, this._onTouchMoved, this, true);
        this.node.off(cc.Node.EventType.TOUCH_END, this._onTouchEnded, this, true);
        this.node.off(cc.Node.EventType.TOUCH_CANCEL, this._onTouchCancelled, this, true);
    }

    setData(index: number) {
        this.index = index;
        this.label.string = index + "";
    }

    private _onTouchBegan(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        if(this.mouseStart) this.mouseStart(event);
    }
    private _onTouchMoved(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        if(this.mouseMove) this.mouseMove(event);
    }

    private _onTouchEnded(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        if(this.mouseEnd) this.mouseEnd(event);
    }

    private _onTouchCancelled(event: cc.Event.EventTouch) {
        if (!this.enabledInHierarchy) return;
        if(this.mouseEnd) this.mouseEnd(event);
    }
    
}
